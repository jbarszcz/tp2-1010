/**
 * @file Main.cpp
 * @author Jean-Alexandre Barszcz
 *
 * Ce programme permet de se familiariser avec les concepts de vector,
 * de constructeurs par copie, de surcharge d'op�rateurs et du
 * pointeur this � l'aide du concept math�matique de vecteur.
 *
 * Suivez les instructions dans la fonction main pour faire
 * fonctionner votre programme.
 */

#include <iostream>
#include <limits>
using namespace std;

#include "Vecteur.h"

int main() {

	// NOTE: Lorsqu'on parle de Vecteur ici, on parle de la classe
	//		 Vecteur qui impl�mente les vecteurs
	//		 math�matiques, et non de la classe vector
	//		 faisant partie de la STL en C++.

	/**************************************************************
	 * Test des constructeurs, de l'op�rateur d'assignation et
	 * d'acc�s
	 *************************************************************/

	// 1 - Cr�er un Vecteur par d�faut (dimension 3).
  Vecteur vecteur;

	// 2 - Cr�er un Vecteur Vec1 = [1, -5, 3, 0, 7].
  float grandeurs[] = {1, -5, 3, 0, 7};
  Vecteur Vec1(5, grandeurs);

	// 3 - Assigner le Vecteur Vec1 au premier vecteur.
  vecteur = Vec1;
	
	// 4 - Modifier la valeur de la seconde composante du premier
	// Vecteur.
  vecteur[1] = numeric_limits<float>::infinity();

	/*************************************************************
	 * Test des diff�rents op�rateurs
	 *************************************************************/

	// 5 - Cr�er un Vecteur Vec2 de taille 5 � partir de l'entr�e
	//	   standard (op�rateur >>).
  Vecteur Vec2(0);
  for(int i=0; i<5; ++i)
    cin >> Vec2;
    
	// 6 - Effectuer l'op�ration suivante : (2.0f * (Vec2 * 5.0f -
	//	   3.0f)) + 1.5f, stocker le r�sultat dans une
	//	   variable et afficher le r�sultat.
  Vecteur yetAnotherVecteur = (2.0f * (Vec2 * 5.0f -  3.0f)) + 1.5f;
  cout << "6: " << yetAnotherVecteur;

	// 7 - Utiliser le r�sultat du #6 et effectuer le produit
	//	   scalaire avec le Vecteur Vec1 et afficher le
	//	   r�sultat.
  float resultat;
  yetAnotherVecteur.produitScalaire(Vec1, resultat);
  cout << "7: " << resultat << endl;

	// 8 - Utiliser les trois op�rations (+=, -=, *=) pour
	//	   modifier le Vecteur Vec2 et afficher le r�sultat.
  cout << "Vec2: " << Vec2;
  cout << "Vec2 += 2.0f: " << (Vec2 += 2.0f);
  cout << "Vec2 -= 4.0f: " << (Vec2 -= 4.0f);
  cout << "Vec2 *= 5.0f: " << (Vec2 *= 5.0f);

	// 9 - Cr�er 2 Vecteurs en 3 dimensions et leur assigner des
	//	   valeurs � partir de l'entr�e standard.
  Vecteur vec3dim1(0);
  Vecteur vec3dim2(0);
  for(int i=0; i<3; ++i)
    cin >> vec3dim1 >> vec3dim2;

	/*************************************************************
	 * Test des fonctions diverses et de l'op�rateur de comparaison
	 *************************************************************/

	// 10 - Utiliser les deux derniers Vecteurs du #9 pour
	//		effectuer un produit vectoriel. Afficher le
	//		r�sultat.
  Vecteur produit;
  vec3dim1.produitVectoriel(vec3dim2, produit);
  cout << "10: " << produit;

	// 11 - Calculer la magnitude de ce nouveau Vecteur et
	// afficher le r�sultat.
  cout << "11: " << produit.magnitude() << endl;

	// 12 - Gr�ce � l'op�rateur de comparaison, afficher "true" si
	//		les Vecteurs Vec1 et Vec2 sont �gaux et
	//		"false" dans le cas contraire.
  cout << "12: " << ((Vec1 == Vec2)?"true":"false") << endl;
	return 0;
}
