# Variables
CC=g++
CFLAGS=-W -Wall -ansi -pedantic
LDFAGS=
EXEC=Driver
OBJECTS=Main.o Vecteur.o

# Targets

all : $(EXEC)

Driver : $(OBJECTS)
	$(CC) -o $@ $^ $(LDFAGS)

%.o : %.cpp 
	$(CC) -o $@ -c $< $(CFLAGS)

# Dependances sur les headers
Main.o : Vecteur.h
Vecteur.o : Vecteur.h

# Phony tagets
.PHONY: clean mrproper

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)
