/***********************************************************************
 * AUTHOR: Jean-Alexandre Barszcz
 *   FILE: .//Vecteur.cpp
 *   DATE: 5 fevrier 2014
 *  DESCR: L'implementation d'un vecteur algebrique
 ***********************************************************************/
#include "Vecteur.h"

#include <cmath>


/**
 * Constucteur qui donne 3 dimensions et initialise chacune a 0.
 */
Vecteur::Vecteur():
    donnees_(vector<float>(3, 0.0f))
{}

/**
 * Cree une Vecteur de la dimension donnee en initialisant toutes ses
 * composantes a 0.
 *
 * \param dimension [in] 	Le nombre de dimensions du nouveau vecteur.
 */
Vecteur::Vecteur(unsigned int dimension):
    donnees_(vector<float>(dimension, 0.0f))
{
}


/**
 * Cree une Vecteur de la dimension donnee en initialisant toutes ses
 * composantes avec les valeurs donnees en paramametre.
 *
 * \param dimension [in] 	Le nombre de dimensions du nouveau vecteur.
 * \param donnees   [in] 	Le tableau des composantes du vecteur.
 */
Vecteur::Vecteur(unsigned int dimension, float donnees[]):
    donnees_(vector<float>(dimension)) //, donnees_[0]) //FIXME
{
    for(unsigned int i=0; i<dimension; ++i)
	donnees_[i] = donnees[i];
}


/**
 * Constructeur de copie. Le nouveau vecteur est de meme dimension et
 * a les memes composantes que celui donne en parametre.
 *
 * \param src [in] 	Le vecteur a copier.
 */
Vecteur::Vecteur(const Vecteur &src)
{
    // L'operateur d'assignation copie le contenu.
    *this = src;
}

/**
 * Destructeur : libere les ressources s'il le faut
 */
Vecteur::~Vecteur()
{
    // Aucune ressource a liberer ou cleanup a faire
}


/**
 * Effectue un produit scalaire entre le vecteur actuel et celui passe
 * en parametre. Cela correspond simplement a la somme des produits
 * des composantes de la meme dimension.
 *
 * \param vec      [in] 	La seconde operande du P.S.
 * \param resultat [out]	La variable dans laquelle on place le resultat
 * \return Vrai si le produit scalaire s'est bien effectue, faux sinon.
 */
bool
Vecteur::produitScalaire(const Vecteur &vec, float &resultat) const
{
    if(vec.getDimension() != getDimension()) return false;
    resultat = 0;
    for(unsigned int i=0; i<getDimension(); ++i)
        resultat += (*this)[i] * vec[i];
    return true;
}

/**
 * Effectue un produit vectoriel entre le vecteur actuel et celui
 * passe en parametre. Le vecteur actuel est la premiere operande et
 * celui passe en parametre est la seconde.
 *
 * \param vec      [in] 	La seconde operande du P.V.
 * \param resultat [out]	La variable dans laquelle on place le resultat
 * \return Vrai si le produit vectoriel s'est bien effectue, faux sinon.
 */
bool
Vecteur::produitVectoriel(const Vecteur &vec, Vecteur &resultat) const
{
    resultat = Vecteur();
    if(getDimension()!=3 || vec.getDimension()!=3) return false;
    for(int i=0; i<3; ++i)
	resultat[i] = (*this)[(i+1)%3] * vec[(i+2)%3] -
	    (*this)[(i+2)%3] * vec[(i+1)%3];
    return true;
}

/**
 * Donne la grandeur du vecteur, ou autrement dit: sqrt( somme ai^2 ).
 *
 * \return La magnitude du vecteur.
 */
float
Vecteur::magnitude() const
{
    float sommeDesCarres = 0;
    for(unsigned int i=0; i<getDimension(); ++i)
	sommeDesCarres += std::pow((*this)[i], 2);
    return std::sqrt(sommeDesCarres);
}


/**
 * Donne au vecteur actuel la meme dimension et les memes composantes
 * que celui donne du cote droit de l'operateur d'affectation.
 *
 * \param rhs     [in] 	Le vecteur dont on prend les valeurs
 * \return Une reference vers le vecteur actuel modifie.
 */
Vecteur &
Vecteur::operator=(const Vecteur &rhs)
{
    // L'operateur d'assignation de la
    // classe vector copie le contenu.
    //    donnees_.resize(rhs.getDimension()); FIXME
    donnees_ = rhs.donnees_;
    return *this;
}


/**
 * Donne acces aux composantes et une possibilite de les modifier.
 *
 * \param index [in] 	L'indice de la composante a rendre
 * \return Une reference vers la composante demandee
 */
float &
Vecteur::operator[](unsigned int index)
{
    return donnees_[index];
}

/**
 * Donne acces aux composantes sans possibilite de les modifier. La methode est constante et garantit aucun changement au Vecteur courant.
 *
 * \param index [in] 	L'indice de la composante a rendre
 * \return La valeur de la composante demandee
 */
float 
Vecteur::operator[] (unsigned int index) const
{
    return donnees_[index];
}

/**
 * Rend un vecteur qui equivaut au vecteur courant auxquel on a ajoute
 * la valeur donnee par parametre a chaque composante.
 *
 * \param f [in] 	La valeur a ajouter a chaque composante
 * \return Un vecteur equivalent a la somme de (*this) et  (f, f, ...)
 */
Vecteur
Vecteur::operator+(float f) const
{
    Vecteur somme(getDimension());
    for(unsigned int i=0; i<getDimension(); ++i)
	somme[i] = (*this)[i] + f;
    return somme;
}

/**
 * Rend un vecteur qui equivaut au vecteur courant auxquel on a
 * soustrait la valeur donnee par parametre a chaque composante.
 *
 * \param f [in] 	La valeur a soustraire de chaque composante
 * \return Un vecteur equivalent a la somme de (*this) et  (-f, -f, ...)
 */
Vecteur
Vecteur::operator-(float f) const
{
    return (*this) + (-f);
}

/**
 * Rend un vecteur qui equivaut au vecteur courant pour lequel on multiplie
 * chaque composante par la valeur donnee par parametre.
 *
 * \param f [in]	La valeur par laquelle multiplier chaque composante
 * \return L'homothetie du vecteur (*this) par un facteur f.
 */
Vecteur
Vecteur::operator*(float f) const
{
    Vecteur produit(getDimension());
    for(unsigned int i=0; i<getDimension(); ++i)
	produit[i] = (*this)[i] * f;
    return produit;
}


Vecteur &
Vecteur::operator+=(float f)
{
    return (*this) = (*this) + f;
}


Vecteur &
Vecteur::operator-=(float f)
{
    return (*this) = (*this) - f;
}


Vecteur &
Vecteur::operator*=(float f)
{
    return (*this) = (*this) * f;
}


bool
Vecteur::operator==(const Vecteur &rhs) const
{
    if(rhs.getDimension() != getDimension()) return false;
    for(unsigned int i=0; i<getDimension(); ++i)
	if(std::abs(rhs[i] - (*this)[i]) > 0.0001f) return false;
    return true;
}


unsigned int
Vecteur::getDimension() const
{
  return donnees_.size();
}

/**
 * Rend un vecteur qui equivaut au vecteur \p vec pour lequel on
 * multiplie chaque composante par la valeur donnee par parametre.
 *
 * \param f   [in]	La valeur par laquelle multiplier chaque composante
 * \param vec [in]	Le vecteur dont on veut obtenir l'homothetie.
 * \return L'homothetie du vecteur (*this) par un facteur f.
 */
Vecteur operator* (float f, const Vecteur& vec)
{
    return vec * f;
}

ostream& operator<< (ostream& os, Vecteur vec)
{
    for(unsigned int i=0; i<vec.getDimension(); ++i)
	os << vec[i] << " ";
    return os << endl;
}

istream& operator>> (istream& in, Vecteur& vec)
{
    float f;
    in >> f;
    vec.donnees_.push_back(f);
    return in;
}
