/**
 * @file Vecteur.h
 * @author
 *
 * Ce fichier contient la définition de la classe Vecteur
 */
#ifndef VECTEUR_H
#define VECTEUR_H

#include <iostream>
#include <vector>
using namespace std;

class Vecteur {
	public:
		Vecteur();
		Vecteur(unsigned int dimension);
		Vecteur(unsigned int dimension, float donnees[]);
		Vecteur(const Vecteur& src);
		~Vecteur();

		bool produitScalaire(const Vecteur& vec, float& resultat) const;
		bool produitVectoriel(const Vecteur& vec, Vecteur& resultat) const;
		float magnitude() const;

		Vecteur& operator= (const Vecteur& rhs);
		float& operator[] (unsigned int index);

		Vecteur operator+ (float f) const;
		Vecteur operator- (float f) const;
		Vecteur operator* (float f) const;

		Vecteur& operator+= (float f);
		Vecteur& operator-= (float f);
		Vecteur& operator*= (float f);

		friend Vecteur operator* (float f, const Vecteur& vec);

		bool operator== (const Vecteur& rhs) const;

		friend ostream& operator<< (ostream& os, Vecteur vec);
		friend istream& operator>> (istream& in, Vecteur& vec);

	private:
		vector<float> donnees_;
};

#endif